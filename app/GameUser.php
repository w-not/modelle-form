<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameUser extends Model
{
    protected $fillable = ['name', 'surname', 'middle_name', 'email', 'token', 'game_status', 'game_started', 'game_ended', 'game_total_time', 'game_total_errors'];

    public function getTime()
    {
        $time = (int) $this->game_total_time;
        $m = floor($time/(60*1000));
        $time -= $m*60*1000;
        $s = floor($time/1000);
        $ms = $time - $s*1000;

        return ($m > 9 ? $m : "0".$m).":".($s > 9 ? $s : "0".$s).":".($ms > 99 ? $ms : ($ms > 9 ? "0".$ms : "00".$ms));
    }
}
