<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    protected $fillable = ['name', 'surname', 'middle_name', 'birthday', 'city_id', 'phone', 'email', 'email_secret', 'unsubscribed'];

    public function city()
    {
        return \App\City::find($this->city_id);
    }
}
