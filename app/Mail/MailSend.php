<?php

namespace App\Mail;

use Config, Mail, Log;
use App\Mail\MailConfig;

class MailSend
{
    public function send($template, $data, $to, $subject, $file=null, $replyTo=null) {
        try {
            Mail::send($template, $data, function ($m) use ($to, $subject, $file, $replyTo) {
                $m->from(Config('mail.from.address'), Config('mail.from.name'));
                $m->to($to['email'], $to['name'])->subject('=?utf-8?B?' . base64_encode($subject) . '?=');
                if($file){
                    $m->attachData($file['raw'], $file['name'], ['as' => $file['name'], 'mime' => $file['mime']]);
                }
                if($replyTo){
                    $m->replyTo($replyTo['email'], $replyTo['name']);
                }
            });
        } catch (\Exception $e) {
            try {
                $mailconfig = new MailConfig;
                $mailconfig->fallback();

                //Rambler Mail stopped loving SendPulse at 08-09.09
                if(strrpos($to['email'],"@rambler") !== false){
                    throw new \Exception("Wake up Neo, Rambler has you", 1);
                }else{
                    Mail::send($template, $data, function ($m) use ($to, $subject, $file, $replyTo) {
                        $m->from(Config('mail.from.address'), Config('mail.from.name'));
                        $m->to($to['email'], $to['name'])->subject('=?utf-8?B?' . base64_encode($subject) . '?=');
                        if($file){
                            $m->attachData($file['raw'], $file['name'], ['as' => $file['name'], 'mime' => $file['mime']]);
                        }
                        if($replyTo){
                            $m->replyTo($replyTo['email'], $replyTo['name']);
                        }
                    });
                }
            } catch (\Exception $e) {
                $mailconfig = new MailConfig;
                $mailconfig->sendmail();
                Mail::send($template, $data, function ($m) use ($to, $subject, $file, $replyTo) {
                    $m->from(Config('mail.from.address'), Config('mail.from.name'));
                    $m->to($to['email'], $to['name'])->subject('=?utf-8?B?' . base64_encode($subject) . '?=');
                    if($file){
                        $m->attachData($file['raw'], $file['name'], ['as' => $file['name'], 'mime' => $file['mime']]);
                    }
                    if($replyTo){
                        $m->replyTo($replyTo['email'], $replyTo['name']);
                    }
                });
                \Log::error('Email send failed, using sendmail. [' . $to['email'] . ', ' . $subject . ']');
            }
        }
    }
    
    
    /*public function set($sender) {
        if(Config('mail.driver') == 'log') {
            return $this;
        }
        $settings = Config('mail.configurations.' . $sender);
        $transport = Swift_SmtpTransport::newInstance(Config('mail.host'), Config('mail.port'), Config('mail.encryption'));
        $transport->setUsername($settings['username']);
        $transport->setPassword($settings['password']);
        $mailer = new Swift_Mailer($transport);
        Mail::setSwiftMailer($mailer);

        Mail::alwaysFrom($settings['username'], Config::get('mail.from.name'));
        config(['mail.from.address' => $settings['username']]);

        return $this;
    }*/
}