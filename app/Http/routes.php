<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'MainController@index');
Route::post('store', 'MainController@store');
Route::get('unsubscribe', 'MainController@unsubscribe');
Route::post('unsubscribe', 'MainController@unsubscribeAction');
Route::post('game_store', 'GameController@store');

Route::get('/test', 'MainController@testNoty');

Route::controllers([
    'auth'    => 'Auth\AuthController',
]);

Route::get('/admin/xls', 'MainController@formXls');

Route::group([
    'prefix'     => 'admin',
    'namespace'  => 'Admin'
], function() {
    Route::get('/', 'AdminController@index');
    //Route::any('/xls', 'AdminController@formXls');
    Route::get('/game', 'AdminController@gameIndex');
    Route::get('/game/csv', 'AdminController@gameCsv');
    Route::get('/game/xls', 'AdminController@gameXls');
    Route::get('/push', 'AdminController@pushIndex');
    Route::post('/push/send_messages', 'AdminController@pushSendMessages');
});

Route::group([
    'prefix'     => 'api',
    'namespace'  => 'Api'
], function() {
    Route::get('/', 'ApiController@index');
    Route::post('/set_device_id', 'ApiController@setDeviceId');
    Route::post('/seen', 'ApiController@setSeen');
});