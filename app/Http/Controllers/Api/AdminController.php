<?php

namespace App\Http\Controllers\Admin;

use App\Form as Item;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->get('filter', []);
        $order  = $request->get('order', 'id');
        $dir    = $request->get('dir');

        $filter_city = $request->get('city');
        $filter_birthday_start = $request->get('birthday_start');
        $filter_birthday_end = $request->get('birthday_end');
        $filter_created_start = $request->get('created_start');
        $filter_created_end = $request->get('created_end');

        $query  = (new Item)->newQuery();

        if ($order){
            $query->orderBy($order, $dir ? 'desc' : 'asc');
        }

        if ($filter_city) $query->where('city_id', '=', $filter_city);
        if ($filter_birthday_start) $query->where('birthday', '>=', $filter_birthday_start);
        if ($filter_birthday_end) $query->where('birthday', '<=', $filter_birthday_end);
        if ($filter_created_start) $query->where('created_at', '>=', $filter_created_start." 00:00:00");
        if ($filter_created_end) $query->where('created_at', '<=', $filter_created_end." 23:59:59");

        $items = $query->paginate(50);
        $items->appends($request->except('page'));

        $cities = \App\City::all();

        return view('admin.index', compact(
            'items', 'filter', 'order', 'dir', 'cities',
            'filter_city', 'filter_birthday_start', 'filter_birthday_end', 'filter_created_start', 'filter_created_end'
        ));
    }
}
