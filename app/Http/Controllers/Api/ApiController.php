<?php

namespace App\Http\Controllers\Api;

use App\Device;
use App\Form as Item;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['setDeviceId','setSeen']);
    }

    public function setSeen(Request $request)
    {
        $device_token = $request->get('device_token');
        if(!$device_token){
            return ['status'=>false,'message' => 'Небыл передан device_token'];
        }

        $device = Device::where(['device_token'=>$device_token])->first();
        if(!$device){
            return ['status'=>false,'message' => 'device_token не найден'];
        }

        $device->badge = 0;
        $device->save();

        return ['status'=>true,'device' => $device];
    }

    public function sendToAServer($request)
    {
        $query = http_build_query($request);
        $params = array(
            'http' => array(
                'header' => "Content-Type: application/x-www-form-urlencoded\r\n"."Content-Length: ".strlen($query)."\r\n"."User-Agent:MyAgent/1.0\r\n",
                'method' => 'POST',
                'content' => $query
            )
        );
        $ctx = stream_context_create($params);
        $fp = fopen('http://modelle.uiux.ru/api/set_device_id', 'rb', false, $ctx);
        if ($fp) {
            $result = @stream_get_contents($fp);
            return json_decode($result, true);
        } else {
            return ['status'=>false,'message' => 'A server error'];
        }
    }

    public function setDeviceId(Request $request)
    {
        return $this->sendToAServer($request->all());
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->get('filter', []);
        $order  = $request->get('order', 'id');
        $dir    = $request->get('dir');

        $filter_city = $request->get('city');
        $filter_birthday_start = $request->get('birthday_start');
        $filter_birthday_end = $request->get('birthday_end');
        $filter_created_start = $request->get('created_start');
        $filter_created_end = $request->get('created_end');

        $query  = (new Item)->newQuery();

        if ($order){
            $query->orderBy($order, $dir ? 'desc' : 'asc');
        }

        if ($filter_birthday_start) $query->where('birthday', '>=', $filter_birthday_start);
        if ($filter_birthday_end) $query->where('birthday', '<=', $filter_birthday_end);
        if ($filter_created_start) $query->where('created_at', '>=', $filter_created_start." 00:00:00");
        if ($filter_created_end) $query->where('created_at', '<=', $filter_created_end." 23:59:59");

        $items = $query->get();

        $arr_out = [];
        foreach($items as $rw)
        {
            $arr_out[] = [
                'id' => $rw->id,
                'name' => $rw->name,
                'surname' => $rw->surname,
                'middle_name' => $rw->middle_name,
                'birthday' => $rw->birthday,
                'city' => $rw->city() ? $rw->city()->name : '',
                'phone' => $rw->phone,
                'email' => $rw->email,
                'created_at' => $rw->created_at
            ];
        }

        return json_encode($arr_out);
    }
}
