<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    protected function toCsv($inputArray, $outputFileName, $delimiter = ';') {
        $inputArray = $this->utf8ToCp1251($inputArray);
        $tempMemory = fopen('php://memory', 'w');
        foreach ($inputArray as $line) {
            fputcsv($tempMemory, $line, $delimiter);
        }
        fseek($tempMemory, 0);
        header('Content-Type: application/csv');
        header('Content-Disposition: attachement; filename="'.htmlspecialchars($outputFileName).'.csv";');
        fpassthru($tempMemory);
    }

    protected function utf8ToCp1251($input) {
        if (is_array($input)) {
            foreach ($input as $key => $value) {
                $input[$key] = $this->utf8ToCp1251($value);
            }
        } else {
            $input = iconv('UTF-8', 'WINDOWS-1251//TRANSLIT', $input);
        }
        return $input;
    }
}
