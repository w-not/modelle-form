<?php

namespace App\Http\Controllers;

use App\Device;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Mail\MailSend;
use Maatwebsite\Excel\Facades\Excel;


class MainController extends Controller
{
    public function index()
    {
        $cities = \App\City::all();

        return view('welcome', compact('cities'));
    }

    public function store(Request $request)
    {
        $data = explode(";",$request->get('data'));
        foreach($data as $rw)
        {
            parse_str($rw, $arr);
            if (count($arr))
            {
                if (isset($arr['birthday_month']) && isset($arr['birthday_day']) && isset($arr['birthday_year']))
                {
                    $birthday = mktime(0,0,0,$arr['birthday_month'], $arr['birthday_day'], $arr['birthday_year']);
                    $birthday = date("Y-m-d", $birthday);
                }
                else $birthday = "0000-00-00 00:00:00";

                if (\App\Form::where('name', '=', $arr['name'])
                        ->where('surname', '=', $arr['surname'])
                        ->where('middle_name', '=', $arr['middle_name'])
                        ->count() == 0)
                {
                    $item = \App\Form::create([
                        "name" => $arr['name'],
                        "surname" => $arr['surname'],
                        "middle_name" => $arr['middle_name'],
                        "birthday" => $birthday,
                        "city_id" => $arr['city'],
                        "phone" => $arr['phone'],
                        "email" => $arr['email'],
                    ]);

                    $key = md5($item->id . "_" . $item->email);

                    $item->email_secret = $key;
                    $item->save();

                    $MailSend = new MailSend();
                    $MailSend->send(
                        'email.template',
                        ["key" => $key],
                        ['email' => $arr['email'], 'name' => $arr['surname'] . ' ' . $arr['name']],
                        'Модэлль - уникальная коллекция контрацептивов'
                    );
                }
            }
        }
    }

    public function unsubscribe(Request $request)
    {
        $form = \App\Form::where('email_secret', '=', $request->get('key'))
            ->where('unsubscribed', '=', '0')
            ->first();

        if ($form)
        {
            return view('unsubscribe', compact('form'));
        }
        else return redirect()->to('http://модельотношений.рф');
    }

    public function unsubscribeAction(Request $request)
    {
        $form = \App\Form::where('email_secret', '=', $request->get('secret'))->first();
        if ($form)
        {
            \DB::table('forms')
                ->where('email', '=', $form->email)
                ->update(['unsubscribed' => 1]);
        }
    }

    public function formXls()
    {
        $items = \App\Form::all();
        $result[] = [
            'Идентификатор',
            'ФИО',
            'Дата рождения',
            'Город',
            'E-mail',
            'Телефон',
            'Дата регистрации'
        ];

        foreach($items as $rw)
        {
            $result[] = [
                $rw->id,
                $rw->surname." ".$rw->name." ".$rw->middle_name,
                $rw->birthday,
                $rw->city()->name,
                $rw->email,
                $rw->phone,
                $rw->created_at
            ];
        }

        $filename = 'MODELLE_Форма_'.date("Y-m-d H-i-s");

        Excel::create($filename, function($excel) use ($result, $filename) {
            $excel->setTitle($filename);
            $excel->sheet(date("Y-m-d H-i-s"), function($sheet) use ($result) {
                $k = 0;
                foreach($result as $rw)
                {
                    $k++;
                    $sheet->row($k, $rw);
                }
            });
        })->store('xls', 'excel/exports');

        return response()->download(public_path('excel/exports/'.$filename.".xls"));

        //->download('xls');
    }
}
