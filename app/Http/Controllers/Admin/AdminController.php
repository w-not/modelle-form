<?php

namespace App\Http\Controllers\Admin;

use App\Device;
use App\Form as Item;
use App\GameUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->get('filter', []);
        $order  = $request->get('order', 'id');
        $dir    = $request->get('dir');

        $filter_city = $request->get('city');
        $filter_birthday_start = $request->get('birthday_start');
        $filter_birthday_end = $request->get('birthday_end');
        $filter_created_start = $request->get('created_start');
        $filter_created_end = $request->get('created_end');

        $query  = (new Item)->newQuery();

        if ($order){
            $query->orderBy($order, $dir ? 'desc' : 'asc');
        }

        if ($filter_city) $query->where('city_id', '=', $filter_city);
        if ($filter_birthday_start) $query->where('birthday', '>=', $filter_birthday_start);
        if ($filter_birthday_end) $query->where('birthday', '<=', $filter_birthday_end);
        if ($filter_created_start) $query->where('created_at', '>=', $filter_created_start." 00:00:00");
        if ($filter_created_end) $query->where('created_at', '<=', $filter_created_end." 23:59:59");

        $items = $query->paginate(50);
        $items->appends($request->except('page'));

        $cities = \App\City::all();

        return view('admin.index', compact(
            'items', 'filter', 'order', 'dir', 'cities',
            'filter_city', 'filter_birthday_start', 'filter_birthday_end', 'filter_created_start', 'filter_created_end'
        ));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function formXls()
    {
        $items = Item::all();
        $result[] = [
            'Идентификатор',
            'ФИО',
            'Дата рождения',
            'Город',
            'E-mail',
            'Телефон',
            'Дата регистрации'
        ];

        foreach($items as $rw)
        {
            $result[] = [
                $rw->id,
                $rw->surname." ".$rw->name." ".$rw->middle_name,
                $rw->birthday,
                $rw->city()->name,
                $rw->email,
                $rw->phone,
                $rw->created_at
            ];
        }

        Excel::create('MODELLE_Форма_'.date("Y-m-d H-i-s"), function($excel) use ($result) {
            $excel->setTitle('MODELLE_Форма_'.date("Y-m-d H-i-s"));
            $excel->sheet(date("Y-m-d H-i-s"), function($sheet) use ($result) {
                $k = 0;
                foreach($result as $rw)
                {
                    $k++;
                    $sheet->row($k, $rw);
                }
            });
        })->download('xls');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function gameIndex(Request $request)
    {
        $filter = $request->get('filter', []);
        $order  = $request->get('order', 'id');
        $dir    = $request->get('dir');

        $query  = (new GameUser)->newQuery();

        if ($order){
            $query->orderBy($order, $dir ? 'desc' : 'asc');
        }

        $items = $query->paginate(50);
        $items->appends($request->except('page'));

        return view('admin.game', compact('items', 'filter', 'order', 'dir'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function gameCsv()
    {
        $items = GameUser::all();
        $result[] = [
            'Идентификатор',
            'ФИО',
            'E-mail',
            'Дата регистрации',
            'Время на прохождение',
            'Правильных ответов'
        ];

        foreach($items as $rw)
        {
            $result[] = [
                $rw->id,
                $rw->surname." ".$rw->name." ".$rw->middle_name,
                $rw->email,
                $rw->created_at,
                $rw->getTime() == "00:00:000" ? 'Игра не пройдена' : '"'.$rw->getTime().'"',
                $rw->getTime() == "00:00:000" ? 'Игра не пройдена' : 98-$rw->game_total_errors
            ];
        }

        $this->toCsv($result, 'MODELLE_Участники_игры_'.date("Y-m-d H:i:s"));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function gameXls()
    {
        $items = GameUser::all();
        $result[] = [
            'Идентификатор',
            'ФИО',
            'E-mail',
            'Дата регистрации',
            'Время на прохождение',
            'Правильных ответов'
        ];

        foreach($items as $rw)
        {
            $result[] = [
                $rw->id,
                $rw->surname." ".$rw->name." ".$rw->middle_name,
                $rw->email,
                $rw->created_at,
                $rw->getTime() == "00:00:000" ? 'Игра не пройдена' : '"'.$rw->getTime().'"',
                $rw->getTime() == "00:00:000" ? 'Игра не пройдена' : 98-$rw->game_total_errors
            ];
        }

        Excel::create('MODELLE_Участники_игры_'.date("Y-m-d H-i-s"), function($excel) use ($result) {
            $excel->setTitle('MODELLE_Участники_игры_'.date("Y-m-d H-i-s"));
            $excel->sheet(date("Y-m-d H-i-s"), function($sheet) use ($result) {
                $k = 0;
                foreach($result as $rw)
                {
                    $k++;
                    $sheet->row($k, $rw);
                }
            });
        })->download('xls');
    }

    public function pushIndex()
    {
        return view('admin.push');
    }

    public function pushSendMessages(Request $request)
    {
        /*$message = \PushNotification::Message($request->get('message'),[
            'badge' => 1,
        ]);
        $token = '803edfd28a5ff93ce711c0da785d9c9be65025adaf537991150e9b0604727692';
        $devicesProd = [];
        $responses = [];
        $devicesProd[] = \PushNotification::Device($token, ['badge' => 1]);
        $devices = \PushNotification::DeviceCollection($devicesProd);
        $collection = \PushNotification::app('iosProd')
            ->to($devices)
            ->send($message);
        foreach ($collection->pushManager as $push) {
            $responses[] = $push->getAdapter()->getResponse();
        }
        return ['status' => 'Сообщение', 'message' => 'Сообщения успешно отправлены', 'responses' => $responses];*/


        $iosDevicesQuery = Device::where(['os' => 'ios']);
        $iosDevices = $iosDevicesQuery->get();
        $devicesDev = [];
        $devicesProd = [];
        $responses = [];
        $message = \PushNotification::Message($request->get('message'),[
            'badge' => 1,
        ]);

        if($iosDevicesQuery->count()){
            foreach ($iosDevices as $device){
                if($device->production == 0){
                    $devicesDev[] = \PushNotification::Device($device->device_token, ['badge' => $device->badge]);
                }else{
                    $devicesProd[] = \PushNotification::Device($device->device_token, ['badge' => $device->badge]);
                }
            }
            if(count($devicesDev)){
                $devices = \PushNotification::DeviceCollection($devicesDev);

                $collection = \PushNotification::app('iosDev')
                    ->to($devices)
                    ->send($message);


                // get response for each device push
                foreach ($collection->pushManager as $push) {
                    $responses[] = $push->getAdapter()->getResponse();
                }
            }

            if(count($devicesProd)){
                $devices = \PushNotification::DeviceCollection($devicesProd);

                $collection = \PushNotification::app('iosProd')
                    ->to($devices)
                    ->send($message);


                // get response for each device push
                foreach ($collection->pushManager as $push) {
                    $responses[] = $push->getAdapter()->getResponse();
                }
            }

            $iosDevicesQuery->update(['badge' => \DB::raw('(`badge` + 1)')]);
        }

        $androidDevicesQuery = Device::where(['os' => 'android']);
        $androidDevices = $androidDevicesQuery->get();
        $devices = [];

        if($androidDevicesQuery->count()){
            foreach ($androidDevices as $device){
                $devices[] = \PushNotification::Device($device->device_token, ['badge' => $device->badge]);

            }
            $devices = \PushNotification::DeviceCollection($devices);

            $collection = \PushNotification::app('androidProd')
                ->to($devices)
                ->send($message);


            // get response for each device push
            foreach ($collection->pushManager as $push) {
                $responses[] = $push->getAdapter()->getResponse();
            }
            $androidDevicesQuery->update(['badge' => \DB::raw('(`badge` + 1)')]);
        }


        return ['status' => 'Сообщение', 'message' => 'Сообщения успешно отправлены', 'responses' => $responses];
    }

}
