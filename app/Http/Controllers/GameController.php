<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class GameController extends Controller
{
    public function index()
    {

    }

    public function store(Request $request)
    {
        $status = $request->get('status');
        if ($status == "043043244243e44043843743e43243043d")
        {
            /*$validUser = \App\GameUser::where('email', '=', $request->get('email'))->count();
            if ($validUser == 0)
            {*/
                $item = \App\GameUser::create([
                    'name' => $request->get('name'),
                    'surname' => $request->get('surname'),
                    'middle_name' => $request->get('middle_name'),
                    'email' => $request->get('email'),
                    'game_status' => '0',
                    'game_started' => '0',
                    'game_ended' => '0',
                    'game_total_time' => '0',
                    'game_total_errors' => '0'
                ]);

                $item->token = md5($item->id.$item->email.$item->game_total_time.$item->game_total_errors);
                $item->save();

                return json_encode([
                    'success' => 1,
                    'user' => md5($item->email),
                    'token' => $item->token
                ]);
            /*}
            else
            {
                return json_encode([
                    'success' => 0,
                    'error' => 'Пользователь с таким e-mail уже зарегистрирован'
                ]);
            }*/
        }
        else if ($status == "044043543744343b44c44243044243e432")
        {
            $user = \App\GameUser::where('token', '=', $request->get('token'))->first();
            $user->game_total_time = $request->get('total_time');
            $user->game_total_errors = $request->get('total_errors');
            $user->save();

            $winners = \App\GameUser::where('game_total_time', '>', '0')
                ->where('game_total_errors', '>', '0')
                ->orderBy('game_total_time', 'asc')
                ->orderBy('game_total_errors', 'desc')
                ->take(10)
                ->get();

            $arr_out = [];
            if ($winners)
            {
                foreach ($winners as $rw)
                {
                    $time = (int) $rw->game_total_time;
                    $m = floor($time/(60*1000));
                    $time -= $m*60*1000;
                    $s = floor($time/1000);
                    $ms = $time - $s*1000;

                    $arr_out['results'][] = [
                        'name' => $rw->surname.' '.mb_substr($rw->name,0,1,"UTF-8").'.'.mb_substr($rw->middle_name,0,1,"UTF-8").'.',
                        'time' => ($m > 9 ? $m : "0".$m).":".($s > 9 ? $s : "0".$s).":".($ms > 99 ? $ms : ($ms > 9 ? "0".$ms : "00".$ms)),
                        'correct' => 98-$rw->game_total_errors
                    ];
                }
            }

            echo json_encode($arr_out);
        }
        else if ($status == "204424404382043f43e43f44244b43a438")
        {
            $user = \App\GameUser::where('token', '=', $request->get('token'))->first();
            $arr_out = [];

            $time = (int) $user->game_total_time;
            $m = floor($time/(60*1000));
            $time -= $m*60*1000;
            $s = floor($time/1000);
            $ms = $time - $s*1000;

            $arr_out['user'] = [
                'time' => ($m > 9 ? $m : "0".$m).":".($s > 9 ? $s : "0".$s).":".($ms > 99 ? $ms : ($ms > 9 ? "0".$ms : "00".$ms)),
                'correct' => $user->game_total_errors
            ];

            $winners = \App\GameUser::where('game_total_time', '>', '0')
                ->where('game_total_errors', '>', '0')
                ->orderBy('game_total_time', 'asc')
                ->orderBy('game_total_errors', 'desc')
                ->take(10)
                ->get();

            if ($winners)
            {
                foreach($winners as $rw)
                {
                    $time = (int) $rw->game_total_time;
                    $m = floor($time/(60*1000));
                    $time -= $m*60*1000;
                    $s = floor($time/1000);
                    $ms = $time - $s*1000;

                    $arr_out['results'][] = [
                        'name' => $rw->surname.' '.mb_substr($rw->name,0,1,"UTF-8").'.'.mb_substr($rw->middle_name,0,1,"UTF-8").'.',
                        'time' => ($m > 9 ? $m : "0".$m).":".($s > 9 ? $s : "0".$s).":".($ms > 99 ? $ms : ($ms > 9 ? "0".$ms : "00".$ms)),
                        'correct' => 98-$rw->game_total_errors
                    ];
                }
            }

            echo json_encode($arr_out);
        }
    }
}
