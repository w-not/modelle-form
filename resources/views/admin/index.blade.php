@extends('admin.layout')

@section('content')
    <!-- begin page-header -->
    <h1 class="page-header">
        Список анкет
        <small>({{$items->total()}})</small>
    </h1>

    {!! Form::open(['method' => 'get', 'action' => 'Admin\AdminController@index']) !!}
    <div class="panel">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="form-group">
                            {!! Form::label('city', 'Город', ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-8">
                                <select id="city" name="city" class="form-control">
                                    <option value="0">Все города</option>
                                    @foreach($cities as $rw)
                                        <option value="{{$rw->id}}" {{isset($filter_city) && $filter_city == $rw->id ? 'selected' : ''}}>{{$rw->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-2">
                                {!! Form::submit('Поиск', ['class' => 'btn btn-primary']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <br/>
                        <div class="form-group">
                            {!! Form::label('birthday_start', 'Дата рождения', ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-8">
                                <div class="input-group input-daterange">
                                    <span class="input-group-addon">от</span>
                                    {!! Form::text('birthday_start', (isset($filter_birthday_start) ? $filter_birthday_start : null), ['class' => 'form-control']) !!}
                                    <span class="input-group-addon">до</span>
                                    {!! Form::text('birthday_end', (isset($filter_birthday_end) ? $filter_birthday_end : null), ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <br/>
                        <div class="form-group">
                            {!! Form::label('created_start', 'Дата регистрации', ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-8">
                                <div class="input-group input-daterange">
                                    <span class="input-group-addon">от</span>
                                    {!! Form::text('created_start', (isset($filter_created_start) ? $filter_created_start : null), ['class' => 'form-control']) !!}
                                    <span class="input-group-addon">до</span>
                                    {!! Form::text('created_end', (isset($filter_created_end) ? $filter_created_end : null), ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

    <div class="panel panel-inverse">
        <div class="panel-heading">
            {!!$items->render()!!}

            <div class="pull-right">
                <a class="btn btn-primary" href="{{action('MainController@formXls')}}">Выгрузить в XLS</a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <?php $columns = [
                                            'id'   => 'ID',
                                            'surname' => 'ФИО',
                                            'birthday' => 'Дата рождения',
                                            'city_id' => 'Город',
                                            'email' => 'E-mail',
                                            'phone' => 'Телефон',
                                            'created_at' => 'Дата регистрации',
                                    ]; ?>
                                    @foreach($columns as $k => $v)
                                        <th
                                                {!! ($k == 'id') ? ' width="60" ' : '' !!}
                                                {!! ($k == 'img') ? ' width="50" ' : '' !!}
                                        >
                                            @if ($k != 'img')
                                                <a href="{{action('Admin\AdminController@index') . '?' . http_build_query([
                                                    'order'  => $k,
                                                    'dir'    => $order == $k ? !$dir : $dir,
                                                    'filter' => $filter,
                                                ])}}">
                                                    @endif
                                                    {{$v}}
                                                    @if ($k != 'img')
                                                        @if ($order != $k)
                                                            <i class="fa fa-sort"></i>
                                                        @elseif ($dir)
                                                            <i class="fa fa-sort-desc"></i>
                                                        @else
                                                            <i class="fa fa-sort-asc"></i>
                                                        @endif
                                                </a>
                                            @endif
                                        </th>
                                    @endforeach

                                    <th width="60">Действия</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($items as $item)
                                    <tr>
                                        <td>{{$item->id}}</td>
                                        <td>{{$item->surname." ".$item->name." ".$item->middle_name}}</td>
                                        <td>{{$item->birthday}}</td>
                                        <td>{{$item->city()->name}}</td>
                                        <td>{{$item->email}}</td>
                                        <td>{{$item->phone}}</td>
                                        <td>{{$item->created_at}}</td>
                                        <td class="right_buttons">
                                            <a class="btn btn-success modal_show" href="" title="Просмотр">
                                                <i class="fa fa-eye"></i>
                                                <input type="hidden" name="surname" value="{{$item->surname}}"/>
                                                <input type="hidden" name="name" value="{{$item->name}}"/>
                                                <input type="hidden" name="middle_name" value="{{$item->middle_name}}"/>
                                                <input type="hidden" name="birthday" value="{{$item->birthday}}"/>
                                                <input type="hidden" name="city" value="{{$item->city()->name}}"/>
                                                <input type="hidden" name="email" value="{{$item->email}}"/>
                                                <input type="hidden" name="phone" value="{{$item->phone}}"/>
                                                <input type="hidden" name="created_at" value="{{$item->created_at}}"/>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            {!!$items->render()!!}
        </div>
    </div>

    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Просмотр</h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </div>

        </div>
    </div>

@endsection

@section('js')
    <script type="text/javascript">
        $(".input-daterange").datepicker({
            todayHighlight:!0,
            format: "yyyy-mm-dd",
            weekStart: 1,
            language: "ru"
        });

        $('.modal_show').click(function(e){
            e.preventDefault();

            $('#myModal .modal-body').html('<p><b>Фамилия:</b> '+$(this).find('[name="surname"]').val()+'</p>'+
                    '<p><b>Имя:</b> '+$(this).find('[name="name"]').val()+'</p>'+
                    '<p><b>Отчество:</b> '+$(this).find('[name="middle_name"]').val()+'</p>'+
                    '<p><b>Дата рождения:</b> '+$(this).find('[name="birthday"]').val()+'</p>'+
                    '<p><b>Город:</b> '+$(this).find('[name="city"]').val()+'</p>'+
                    '<p><b>E-mail:</b> '+$(this).find('[name="email"]').val()+'</p>'+
                    '<p><b>Телефон:</b> '+$(this).find('[name="phone"]').val()+'</p>'+
                    '<p><b>Согласен на обработку персональных данных:</b> да</p>'+
                    '<p><b>Дата регистрации:</b> '+$(this).find('[name="created_at"]').val()+'</p>');

            $('#myModal').modal('show');
        });
    </script>
@endsection