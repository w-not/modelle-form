@extends('admin.layout')

@section('js')
    <script>
        $(document).ready(function(){
            $('.messageForm').on('submit',function(e){
                e.preventDefault()
                var messageInput = $('.messageInput').val() || false

                if(!messageInput){
                    $("<div title='Ошибка'>Введите текст сообщения</div>").dialog({
                        draggable: false,
                        resizable: false
                    })
                    return false
                }

                var target = $(this).closest('.panel');
                var targetBody = $(target).find('.panel-body');
                var spinnerHtml = '<div class="panel-loader"><span class="spinner-small"></span></div>';
                $(target).addClass('panel-loading');
                $(targetBody).prepend(spinnerHtml);

                $.ajax({
                    method:'POST',
                    url: '/admin/push/send_messages',
                    data: {message:messageInput},
                    success: function(data){
                        $(target).removeClass('panel-loading');
                        $(target).find('.panel-loader').remove();
                        $("<div title='"+data.status+"'>"+data.message+"</div>").dialog({
                            draggable: false,
                            resizable: false
                        })
                    },
                    error:function(){
                        $(target).removeClass('panel-loading');
                        $(target).find('.panel-loader').remove();
                    }
                })

            })
        })
    </script>
@endsection

@section('content')
    <!-- begin page-header -->
    <h1 class="page-header">
        Рассылка Push нотификаций на мобильные устройства

    </h1>

    <div class="panel panel-inverse">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success"
                   data-click="panel-reload" data-original-title="" title=""><i class="fa fa-repeat"></i></a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <form class="messageForm">
                            <div class="form-group">
                                <label class="form-control-label" >Тест сообщения</label>
                                <input type="text" class="form-control messageInput" placeholder="Введите текст" maxlength="100">
                                <small id="emailHelp" class="form-text text-muted">Тест сообщения, который получат пользователи в виде Push уведомления</small>
                            </div>
                            <button type="submit" class="btn btn-primary">Отправить всем</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection