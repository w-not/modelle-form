{{--
 User: Michael Lazarev <mihailaz.90@gmail.com>
 Date: 26.10.15
 Time: 16:48
--}}

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (session()->has('valid_error'))
    <div class="alert alert-danger">
        {{session()->get('valid_error')}}
    </div>
@endif

@if (session()->has('message'))
    <div class="alert alert-{{session()->get('message_level', 'success')}}">
        {{session()->get('message')}}
    </div>
@endif

@if (session()->has('status'))
    <div class="alert alert-success">
        {{session('status')}}
    </div>
@endif