@extends('admin.layout')

@section('content')
    <!-- begin page-header -->
    <h1 class="page-header">
        Участники игры
        <small>({{$items->total()}})</small>
    </h1>

    <div class="panel panel-inverse">
        <div class="panel-heading">
            {!!$items->render()!!}

            <div class="pull-right">
                <a class="btn btn-primary" href="{{action('Admin\AdminController@gameCsv')}}">Выгрузить в CSV</a>
                <a class="btn btn-primary" href="{{action('Admin\AdminController@gameXls')}}">Выгрузить в XLS</a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <?php $columns = [
                                            'id'   => 'ID',
//                                            'surname' => 'ФИО',
//                                            'email' => 'E-mail',
                                            'created_at' => 'Дата регистрации',
                                            'game_total_time' => 'Время на прохождение',
                                            'game_total_errors' => 'Правильных ответов',
                                    ]; ?>
                                    @foreach($columns as $k => $v)
                                        <th
                                                {!! ($k == 'id') ? ' width="60" ' : '' !!}
                                                {!! ($k == 'img') ? ' width="50" ' : '' !!}
                                        >
                                            @if ($k != 'img')
                                                <a href="{{action('Admin\AdminController@gameIndex') . '?' . http_build_query([
                                                    'order'  => $k,
                                                    'dir'    => $order == $k ? !$dir : $dir,
                                                    'filter' => $filter,
                                                ])}}">
                                                    @endif
                                                    {{$v}}
                                                    @if ($k != 'img')
                                                        @if ($order != $k)
                                                            <i class="fa fa-sort"></i>
                                                        @elseif ($dir)
                                                            <i class="fa fa-sort-desc"></i>
                                                        @else
                                                            <i class="fa fa-sort-asc"></i>
                                                        @endif
                                                </a>
                                            @endif
                                        </th>
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($items as $item)
                                    <tr>
                                        <td>{{$item->id}}</td>
{{--                                        <td>{{$item->surname." ".$item->name." ".$item->middle_name}}</td>--}}
                                        {{--<td>{{$item->email}}</td>--}}
                                        <td>{{$item->created_at}}</td>
                                        <td>{{$item->getTime()}}</td>
                                        <td>{{$item->getTime() != "00:00:000" ? 90-$item->game_total_errors : 'Игра не пройдена'}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            {!!$items->render()!!}
        </div>
    </div>
@endsection