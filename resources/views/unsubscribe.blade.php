<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MODELLE</title>
    <link rel="shortcut icon" href="/favicon.png">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700&amp;subset=cyrillic" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&amp;subset=cyrillic" rel="stylesheet">
    <link href="/desktop.bundle.css?v={{config('app.version')}}" rel="stylesheet">
</head>
<body class="unsubscribe_page">
    <div class="unsubscribe_page_wrap">
        <h2>Вы действительно хотите<br/>отписаться от данной рассылки?</h2>
        <p>Для того, чтобы отписаться от<br/>рассылки, нажмите <a href="#">здесь</a>.</p>
    </div>
    <input type="hidden" id="key" value="{{$form->email_secret}}"/>

{{--<header>
    <div class="logo">
        <img src="/images/logo.png"/>
    </div>
</header>

<div class="unsubscribe">
    <h2>Вы действительно хотите отписаться от рассылки с сайта <a href="http://модельотношений.рф">модельотношений.рф</a></h2>
    <div class="button_row">
        <div class="button ok">Да</div>
        <a href="http://модельотношений.рф" class="button cancel">Нет</a>
    </div>
</div>

<footer>
    <div class="f_copy">© 2008-2017 ООО «Тева»</div>
    <div class="f_small">Информация на данном сайте предназначена для медицинских работников.</div>
</footer>--}}

<div id="modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
                <p>Some text in the modal.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
            </div>
        </div>

    </div>
</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="/assets/plugins/masked-input/masked-input.min.js?v={{config('app.version')}}"></script>
<script src="/jquery.cookie.js?v={{config('app.version')}}"></script>
<link href="/node_modules/select2/dist/css/select2.min.css?v={{config('app.version')}}" rel="stylesheet">
<script src="/node_modules/select2/dist/js/select2.full.min.js?v={{config('app.version')}}"></script>
<script src="/desktop.bundle.js?v={{config('app.version')}}"></script>
</body>
</html>
