<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Модэлль</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body bgcolor="#ffffff" style="padding: 0; margin: 0;">

<table width="600" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" style="margin: 0 auto;">

    <tr>
        <td width="600" valign="top">
            <table width="600" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" align="center">
                <tr>
                    <td width="600" height="293" colspan="2" valign="top">
                        <img src="http://modelle-form.uiux.ru/images/email/index_01.png" width="600" height="293" border="0" alt="Уважаемый коллега!" style="display: block;" />
                    </td>
                </tr>

                <tr>
                    <td width="300" height="145" valign="top">
                        <img src="http://modelle-form.uiux.ru/images/email/index_02.png" width="300" height="145" alt="" style="display: block;" />
                    </td>
                    <td width="300" height="145" valign="top">
                        <p style="margin-top: 0; margin-bottom: 0; font-weight: normal; font-size: 20px; line-height: 140%; font-family: Arial; color: #414141;">Благодарим Вас за<br> проявленный интерес к<br> бренду Модэлль.</p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td width="600" valign="top">
            <table width="600" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" align="center">
                <tr>
                    <td width="600" colspan="3" valign="top">
                        <img src="http://modelle-form.uiux.ru/images/email/index_05.png" width="600" height="112" alt="Модэлль — уникальная1 коллекция контрацептивов," style="display: block;" />
                    </td>
                </tr>

                <tr>
                    <td width="67" height="122" valign="top">
                        <img src="http://modelle-form.uiux.ru/images/email/index_06.png" width="67" height="122" alt="" style="display: block;" />
                    </td>
                    <td width="467" height="122" valign="top" style="background: #652981;" bgcolor="#652981">
                        <p style="margin-top: 0; margin-bottom: 0; font-weight: normal; font-size: 19px; line-height: 150%; font-family: Arial; color: #ffffff;">которая оберегает настоящую любовь<br> от незапланированных перемен<sup style="font-size: 60%; line-height: 100%;">2</sup>, учитывая индивидуальные потребности женщины<br> в разные периоды ее сексуальной жизни</p>
                    </td>
                    <td width="66" height="122" valign="top">
                        <img src="http://modelle-form.uiux.ru/images/email/index_08.png" width="66" height="122" alt="" style="display: block;" />
                    </td>
                </tr>

                <tr>
                    <td width="600" height="32" colspan="3" valign="top">
                        <img src="http://modelle-form.uiux.ru/images/email/index_09.png" width="600" height="32" alt="" style="display: block;" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td width="600" valign="top">
            <p style="margin-top: 20px; margin-right: 25px; margin-bottom: 10px; margin-left: 25px; font-weight: normal; font-size: 20px; line-height: 140%; font-family: Arial; color: #414141;">Теперь у Вас есть уникальный доступ к сайту<br> <a href="http://www.модельотношений.рф/" style="font-weight: bold; color: #5c2077;">www.модельотношений.рф</a>, содержащий свежую<br> информацию из мира акушерства и гинекологии, <br>а именно:</p>

            <ul style="margin-left: 0; padding-left: 15px;">
                <li style="margin-top: 0; margin-right: 25px; margin-bottom: 5px; margin-left: 25px; font-weight: normal; font-size: 20px; line-height: 140%; font-family: Arial; color: #414141;">Публикации медицинских исследований</li>
                <li style="margin-top: 0; margin-right: 25px; margin-bottom: 5px; margin-left: 25px; font-weight: normal; font-size: 20px; line-height: 140%; font-family: Arial; color: #414141;">Материалы с главных конференций<br> по всей России </li>
                <li style="margin-top: 0; margin-right: 25px; margin-bottom: 5px; margin-left: 25px; font-weight: normal; font-size: 20px; line-height: 140%; font-family: Arial; color: #414141;">Авторские доклады и презентации</li>
                <li style="margin-top: 0; margin-right: 25px; margin-bottom: 0; margin-left: 25px; font-weight: normal; font-size: 20px; line-height: 140%; font-family: Arial; color: #414141;">Свежие новости мира акушерства и гинекологии</li>
            </ul>
        </td>
    </tr>

    <tr>
        <td width="600" valign="top">
            <table width="600" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" align="center">
                <tr>
                    <td width="260" height="322" valign="top">
                        <img src="http://modelle-form.uiux.ru/images/email/index_14.png" width="260" height="322" alt="" style="display: block;" />
                    </td>
                    <td width="340" height="322" valign="top">
                        <p style="margin-top: 20px; margin-bottom: 15px; font-weight: normal; font-size: 20px; line-height: 150%; font-family: Arial; color: #414141;">Кроме того, на нашем сайте Вы<br> найдете актуальный <a href="http://модельотношений.рф/events" style="color: #662483; text-decoration: none;">календарь<br> значимых мероприятий</a> из области<br> акушерства и гинекологии, а также<br> алгоритм<sup style="font-size: 60%; line-height: 100%;">3</sup> подбора контрацепции<sup style="font-size: 60%; line-height: 100%;">4</sup><br> – <a href="http://модельотношений.рф/algorithm" style="color: #662483; text-decoration: none;">«Модель выбора контрацепции»</a>,<br> разработанный ведущими<br> гинекологами РОАГ и РУДН3.</p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td width="600" height="50" valign="top" align="center">
            <p style="margin-top: 0; margin-bottom: 0; font-weight: normal; font-size: 20px; line-height: 150%; font-family: Arial; color: #414141;">Будем рады видеть Вас снова!</p>
        </td>
    </tr>

    <tr>
        <td width="600" height="35" valign="top" align="center">
            <table width="260" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" align="center">
                <tr>
                    <td width="260">
                        <p style="margin-top: 0; margin-bottom: 15px;">
                            <a href="http://модельотношений.рф"><img src="http://modelle-form.uiux.ru/images/email/index_18.png" width="253" height="65" border="0" alt="Перейти на сайт" /></a>
                        </p>

                        <p style="margin-top: 0; margin-bottom: 10px; font-weight: normal; font-size: 20px; line-height: 140%; font-family: Arial; color: #414141;">С уважением,<br> Команда бренда Модэлль</p>
                        <p style="margin-top: 0; margin-bottom: 25px; font-weight: normal; font-size: 15px; line-height: 130%; font-family: Arial; color: #414141;">Чтобы отписаться от этой рассылки,<br> перейдите по <a href="http://modelle-form.uiux.ru/unsubscribe?key={{$key}}" style="color: #414141;">ссылке</a></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td width="600" valign="top" style="background: #ebebeb;" bgcolor="#ebebeb">
            <table width="550" border="0" cellpadding="0" cellspacing="0" align="center">
                <tr>
                    <td width="550" colspan="2" valign="top">
                        <p style="margin-top: 20px; margin-bottom: 22px; border-bottom: 1px solid #d7d7d7; padding-bottom: 20px; font-weight: normal; font-size: 15px; line-height: 130%; font-family: Arial; color: #414141;"><sup style="font-size: 60%; line-height: 100%;">1</sup> Единственная линейка контрацептивов в РФ под единым «зонтичным брендом». Единственная линейка контрацептивов в РФ, в составе которой одновременно препараты с МНН дезогестрел 0,075 мг, ципротерон 2 мг + этинилэстрадиол 0,035 мг, дроспиренон 3 мг + этинилэстрадиол 0,03 мг, дроспиренон 3 мг + этинилэстрадиол 0,02 мг, левоноргестрел 0,1 мг + этинилэстрадиол 0,02 мг и этинилэстрадиол 0,01 мг, http://grls.rosminzdrav.ru/  29.05.2017.<br> <sup style="font-size: 60%; line-height: 100%;">2</sup> От незапланированной беременности.<br> <sup style="font-size: 60%; line-height: 100%;">3</sup> Алгоритм выбора метода контрацепции. Консультирование по контрацепции как основа персонифицированного выбора: информационный бюллетень / В.Е. Радзинский, М.Б. Хамошина. — М.: Редакция журнала StatusPraesens, 2017. — 20 с.<br> <sup style="font-size: 60%; line-height: 100%;">4</sup> Внутри линейки МОДЭЛЛЬ. Материал предназначен для специалистов здравоохранения. Не для демонстрации пациентам.</p>
                    </td>
                </tr>

                <tr>
                    <td width="86" valign="top">
                        <a href="http://www.teva.ru">
                            <img src="http://modelle-form.uiux.ru/images/email/index_22.png" width="86" height="86" alt="TEVA" border="0" style="display: block;" />
                        </a>
                    </td>
                    <td>
                        <p style="margin-top: 0px; margin-bottom: 35px; margin-left: 17px; font-weight: normal; font-size: 15px; line-height: 130%; font-family: Arial; color: #414141;">Общество с ограниченной ответственностью «Тева»<br> Россия, 115054, Москва, ул. Валовая, д.35<br> Тел. +74656442234. Факс +74956442235<br> <a href="http://www.teva.ru" style=" color: #414141; text-decoration: none;">www.teva.ru</a><br> MLIB-RU-00045-DOK</p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

</table>

</body>
</html>