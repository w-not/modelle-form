<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MODELLE</title>
    <link rel="shortcut icon" href="/favicon.png">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700&amp;subset=cyrillic" rel="stylesheet">
    <link href="/desktop.bundle.css?v={{config('app.version')}}" rel="stylesheet">
<body>

<header>
    {{--<div class="logo">
        <img src="/images/logo.png"/>
    </div>--}}
</header>

<div class="form">
    <form>
        <div class="form_row">
            <div class="form_input">
                <label for="surname">Фамилия<span>*</span>:</label>
                <input type="text" name="surname" id="surname" data-required="true" />
                <p class="error">обязательное поле</p>
            </div>
            <div class="form_input">
                <label for="name">Имя<span>*</span>:</label>
                <input type="text" name="name" id="name" data-required="true" />
                <p class="error">обязательное поле</p>
            </div>
        </div>
        <div class="form_row">
            <div class="form_input">
                <label for="middle_name">Отчество<span>*</span>:</label>
                <input type="text" name="middle_name" id="middle_name" data-required="true" />
                <p class="error">обязательное поле</p>
            </div>
        </div>
        <div class="form_row">
            <div class="form_date">
                <label for="birthday_day">Дата рождения:</label>
                <select name="birthday_day" id="birthday_day">
                    <option disabled selected value="0">День</option>
                    @for($i = 1; $i <= 31; $i++)
                        <option value="{{$i}}">{{$i}}</option>
                    @endfor
                </select>
                <select name="birthday_month" id="birthday_month">
                    <option disabled selected value="0">Месяц</option>
                    @for($i = 1; $i <= 12; $i++)
                        <option value="{{$i}}">{{$i}}</option>
                    @endfor
                </select>
                <select name="birthday_year" id="birthday_year">
                    <option disabled selected value="0">Год</option>
                    @for($i = date("Y") - 17; $i >= 1900; $i--)
                        <option value="{{$i}}">{{$i}}</option>
                    @endfor
                </select>
            </div>
        </div>
        <div class="form_row">
            <div class="form_input">
                <label for="middle_name">Город:</label>
                <select name="city" id="city" class="js-example-basic-single js-states">
                    @foreach($cities as $rw)
                        <option value="{{$rw->id}}">{{$rw->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form_row">
            <div class="form_input">
                <label for="phone">Телефон:</label>
                <input type="text" name="phone" id="phone" />
                <p class="error">обязательное поле</p>
            </div>
            <div class="form_input">
                <label for="email">E-mail<span>*</span>:</label>
                <input type="text" name="email" id="email" data-required="true" />
                <p class="error">обязательное поле</p>
            </div>
        </div>
        <div class="form_row">
            <div class="form_checkbox">
                <label for="agreement"><input type="checkbox" id="agreement" name="agreement" /><span><a href="#" id="agreement_link">Согласен на обработку персональных данных</a><span>*</span></span></label>
                <p class="tip"><span>*</span> Обязательные поля</p>
            </div>
        </div>
        <hr/>
        <div class="form_row_buttons">
            <button class="btn_purple">Отправить данные</button>
            <button class="btn_gray">Сохранить</button>
            <input type="hidden" id="submit_type"/>
        </div>
    </form>
</div>

<footer>
    <div class="f_copy">© 2008-2017 ООО «Тева»</div>
    <div class="f_small">Информация на данном сайте предназначена для медицинских работников.</div>
</footer>



<div id="modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
                <p>Some text in the modal.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
            </div>
        </div>

    </div>
</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="/assets/plugins/masked-input/masked-input.min.js?v={{config('app.version')}}"></script>
<script src="/jquery.cookie.js?v={{config('app.version')}}"></script>
<link href="/node_modules/select2/dist/css/select2.min.css?v={{config('app.version')}}" rel="stylesheet">
<script src="/node_modules/select2/dist/js/select2.full.min.js?v={{config('app.version')}}"></script>
<script src="/desktop.bundle.js?v={{config('app.version')}}"></script>
</body>
</html>

