<form method="post" action="{{action('HomeController@anyVote', $id, false)}}" id="voteSend">
    <h3>Sign up</h3>

    <div class="inp-box">
        <div class="inp-group">
            <input type="text" name="first_name" placeholder="FIRST NAME" class="inp-style">
        </div><!--.inp-group-->

        <div class="inp-group">
            <input type="text" name="last_name" placeholder="LAST NAME" class="inp-style">
        </div><!--.inp-group-->

        <div class="inp-group">
            <input type="text" name="email" placeholder="EMAIL ADDRESS" class="inp-style">
        </div><!--.inp-group-->

        <div class="inp-date">

            <label>Date of birth:</label>

            <div class="inp-group">
                <select name="day_birth" class="inp-decorate">
                    <option disabled="" selected="">Day</option>
                    @for($i=1; $i<=31; $i++)
                        <option value="{{$i}}">{{$i}}</option>
                    @endfor
                </select>
            </div><!--.inp-group-->

            <div class="inp-group">
                <select name="mont_birth" class="inp-decorate">
                    <option disabled="" selected="">Month</option>
                    @for($i=1; $i<=12; $i++)
                        <option value="{{$i}}">{{$i}}</option>
                    @endfor
                </select>
            </div><!--.inp-group-->

            <div class="inp-group">
                <select name="year_birth" class="inp-decorate">
                    <option disabled="" selected="">Year</option>
                    @for($i=2009; $i>=1900; $i--)
                        <option value="{{$i}}">{{$i}}</option>
                    @endfor
                </select>
            </div><!--.inp-group-->

        </div><!--.inp-date-->

        <div class="inp-group">
            <select name="country" class="inp-decorate inp-fw">
                <option disabled="" selected="">Select country</option>
                @foreach($countries as $code => $country)
                    <option value="{{$code}}">{{$country}}</option>
                @endforeach
            </select>
        </div><!--.inp-group-->

    </div><!--.inp-box-->

    <h4>Privacy Consent:  By submitting this form you expressly consent that the Manchester United (“MU”) Group (being all companies with the MU name) and the other MU Commercial Partners (as both may change over time) may share &amp; use your personal information (a) to provide products &amp; services you request, (b) for consumer profiling, market research &amp; (c) unless you tick the boxes below to contact you by post, phone or electronically (incl. email, text &amp; DigitalTV) about MU related products, services, offers &amp; events.</h4>

    <p>Don’t send me details of products, services, offers &amp; events from</p>

    <div class="inp-group inp-chkb">
        <div class="inp-line">
            <label><input type="checkbox" name="disagree_group" value="1"> MU Group</label>
        </div><!--.inp-line-->

        <div class="inp-line">
            <label><input type="checkbox" name="disagree_partners" value="1"> MU Commercial Partners</label>
        </div><!--.inp-line-->
    </div><!--.inp-group-->

    <h4>Please click here to read the <a href="http://www.manutd.com/en/General-Footer-Section/Privacy-Policy.aspx" target="_blank">MU Privacy Policy</a> and fo ran updated list of <a href="http://www.manutd.com/en/General-Footer-Section/Privacy-Policy.aspx?filter=companies" target="_blank">MU Group companies</a> &amp; <a href="http://www.manutd.com/en/General-Footer-Section/Privacy-Policy.aspx?filter=partners" target="_blank">MU Commercial Partners</a>.</h4>


    <input type="submit" value="Send">
</form>