<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('surname')->nullable();
            $table->string('middle_name')->nullable();
            $table->string('email')->nullable()->unique();
            $table->string('token')->nullable()->unique();
            $table->integer('game_status')->default(0);
            $table->integer('game_started')->default(0);
            $table->integer('game_ended')->default(0);
            $table->integer('game_total_time')->nullable();
            $table->integer('game_total_errors')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('game_users');
    }
}
