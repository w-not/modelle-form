<?php

use Illuminate\Database\Seeder;

class RemoveDublicates extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $forms = \App\Form::orderBy('id', 'desc')->get();

        $unique = [];
        $unique_id = [];

        foreach($forms as $rw)
            if (!in_array($rw->surname." ".$rw->name." ".$rw->middle_name, $unique))
            {
                $unique[] = $rw->surname." ".$rw->name." ".$rw->middle_name;
                $unique_id[] = $rw->id;
            }

        foreach($unique_id as $rw)
        {
            echo $rw.",";
        }

        //dd(count($unique));
    }
}
