<?php

use Illuminate\Database\Seeder;

class RestoreGame extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = \App\GameUser::where('game_total_time', '>', '599999')
            ->get();

        foreach($users as $rw)
        {
            $rw->game_total_time -= 60000;
            echo $rw->id." ".$rw->getTime()."\n";
            $rw->save();

        }

        /*foreach($users as $rw)
        {
            $time = strtotime($rw->updated_at) - strtotime($rw->created_at);
            $mili = substr($rw->game_total_time, -3);
            if ($mili == "999") $mili = str_pad(rand(0, 999), 3, "0", STR_PAD_LEFT);

            $rw->game_total_time = $time.$mili;
            $rw->save();
            //echo $rw->id." ".$time.$mili."\n";
        }*/
    }
}
