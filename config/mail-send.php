<?php

return [
	// Путь по которому расположена админка
	'admin-route'   => 'admin',

	// Используемый шаблон
	'admin-layout'  => 'admin.layout.admin-layout',

	// Секция в которой размещается контент страницы
	'admin-section' => 'content',

	// Добавление почтовых ящиков для отправки
	'mailer' => [
		'yandex' => [
			'host'          => 'smtp.yandex.ru',
			'port'          => 465,
			'encryption'    => 'SSL',
			'username'      => 'noreply@prazdnik.promo',
			'password'      => 'CFs^DG!7ca%q621HCH',
			'from'          => [
				'address' => 'noreply@prazdnik.promo',
				'name'    => 'Праздники продолжаются'
			],
		]
		//...
	]
];