<?php

return array(

    'iosDev'     => array(
        'environment' =>'development',
        'certificate' => app_path().'/IosPushDev.pem',
        'passPhrase'  =>'2450440',
        'service'     =>'apns'
    ),
    'iosProd'     => array(
        'environment' =>'production',
        'certificate' => app_path().'/IosPushDistr.pem',
        'passPhrase'  =>'2450440',
        'service'     =>'apns'
    ),
    'androidProd' => array(
        'environment' =>'production',
        'apiKey'      =>'AAAAKbDmzo4:APA91bG_8dkp_-vdOoCMOsazNUg20GNJJuiKpaciWDL2ZmhGh8Au99xX9qvYpLp8d1mwI9zXkjU9LjOcOlfWBeh9h4FgowuZJvpaNVmuT87AfpWYTsFMbxxC1z5fsmL5EZR8b57MAPhZ',
        'service'     =>'gcm'
    )

);