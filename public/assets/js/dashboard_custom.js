/**
 * User: Michael Lazarev <mihailaz.90@gmail.com>
 * Date: 28.10.15
 * Time: 15:08
 */

(function(){
	$(document).on('click', '[data-crop]', function(e){
		e.preventDefault();

		var btn    = $(this),
			img    = $(btn.data('crop')),
			form   = $(img.data('crop-form')),
			inputs = {};

		if (img.data('cropper-init')){
			form.submit();
			return;
		}
		btn.text(btn.data('save-text'));
		img.data('cropper-init', true);

		form.find(':input[name]').each(function(){
			var el = $(this);
			inputs[el.attr('name')] = el;
		});
		img.cropper({
			build: function(){
				console.log('build');
			},
			crop: function(e){
				console.log('crop');
				inputs.x.val(e.x);
				inputs.y.val(e.y);
				inputs.w.val(e.width);
				inputs.h.val(e.height);
			}
		});
	});
})();

(function(){
	 

	var modal,
	    row,
	    updated = false;

	$(document).on('click', '[data-modal]', function(e){
		e.preventDefault();
		var btn = $(this);
		if (!modal){
			modal = new Modal();
		}
		updateRow(btn.parents('[data-row]'));
		return false;
	});

	$(document).on('click', '[data-status]', function(){
		var btn    = $(this),
		    form   = btn.parents('.modal').find('.modal-body form'),
		    data   = form.serializeArray(),
		    status = btn.attr('data-status');

		if (btn.is('[data-confirmable]') && !confirm('Вы уверены, что чек уникален? Такой номер уже существует в системе!')){
			return;
		}
		btn.removeAttr('data-confirmable');
		form.find('[data-validation]').remove();
		data.push({name: 'status', value: status});

		$.ajax({
			type      : form.attr('method'),
			url       : form.attr('action'),
			data      : data,
			success   : function(){
				var next;
				row.attr('data-row', status);
				next = row.siblings('[data-row=new]:first');
				updated = true;

				if (next.length){
					updateRow(next);
				} else {
					location.reload();
				}
			},
			statusCode: {
				422: function(data){
					data = JSON.parse(data.responseText);
					$.each(data, function(name, msgs){
						var alert = $('<div class="alert alert-danger" data-validation></div>'),
						    ul    = $('<ul class="list-unstyled"></ul>').appendTo(alert);
						form.find('[name=' + name + ']').parent().after(alert);
						$.each(msgs, function(_, msg){
							ul.append('<li>' + msg + '</li>');
						});
					});
				}
			}
		});
	});

	//$(document).on('blur', '[data-receipt-check]', function(){
	//	var inp = $(this),
	//	    val = inp.val();
	//
	//	if (!val){
	//		return;
	//	}
	//	$.ajax({
	//		type   : 'get',
	//		data   : {
	//			receipt_no: val
	//		},
	//		url    : '/admin/receipt-check',
	//		success: function(data){
	//			inp.parent().siblings('[data-receipt-status]').remove();
	//			var status = inp.parents('.modal').find('[data-status=approve]');
	//			if (data.count){
	//				status.attr('data-confirmable', 1);
	//				inp.parent().after('<div class="alert alert-warning" data-receipt-status>Такой номер <u>уже существует в системе</u></div>');
	//			} else {
	//				inp.parent().after('<div class="alert alert-success" data-receipt-status>Номер уникален</div>');
	//			}
	//		}
	//	});
	//});

	$(document).on('click', '[data-modal-size-toggle]', function(e){
		e.preventDefault();
		$(this).toggleClass('img-lg').parents('.modal-dialog').toggleClass('modal-lg');
		return false;
	});

	$(document).on('hidden.bs.modal', '.modal', function(e){
		if (updated){
			location.reload();
		}
	});

	function Modal(append_to){
		this.el = $('<div class="modal fade"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button><h4 class="modal-title"></h4></div><div class="modal-body"></div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button><button type="button" class="btn btn-danger" data-status="reject">Отклонить</button><button type="button" class="btn btn-primary" data-status="approve">Принять</button></div></div></div></div>');
		this.options = {};
		if (!append_to){
			append_to = $(document.body);
		}
		append_to.append(this.el);
	}

	$.extend(Modal.prototype, {
		_title : function(title){
			this.el.find('.modal-title').text(title);
			return this;
		},
		_header: function(header){
			this.el.find('.modal-header').html(header);
			return this;
		},
		_body  : function(body){
			this.el.find('.modal-body').html(body);
			return this;
		},
		_footer: function(footer){
			this.el.find('.modal-footer').html(footer);
			return this;
		},
		_url   : function(options){
			var self = this,
			    url  = (typeof options == 'string') ? options : options.url;
			$.ajax({
				type   : 'get',
				url    : url,
				success: function(data){
					self._body(data);
					if (options.callback){
						options.callback();
					}
				}
			});
			return this;
		},
		open   : function(options){
			var self = this;
			$.each(options, function(k, v){
				self['_' + k](v);
			});
			this.options = options;
			this.el.modal('show');
			this.el.find('data-confirmable').removeAttr('data-confirmable');
			return this;
		},
		close  : function(){
			this.el.modal('hide');
			return this;
		}
	});
	function updateRow(r){
		var btn = r.find('[data-modal]:first');
		row = r;

		modal.open({
			title: btn.attr('data-modal'),
			url  : btn.attr('href')
		});
	}

	$(document).on('submit', '[data-filter]', function(e){
		e.preventDefault();
		var form   = $(this),
		    action = form.attr('action'),
		    data   = {};

		$.each(form.serializeArray(), function(_, v){
			if (!data[v.name]){
				data[v.name] = [];
			}
			data[v.name].push(v.value);
		});
		$.each(data, function(k, v){
			action = action.replace(':' + k + ':', v.join(','));
		});
		location.href = action;

		return false;
	});

	$('.check').click(function(){
		var arr = new Array();
		$('.check').each(function(){
			if ($(this).is(':checked'))
			{
				arr.push($(this).attr('data-id'));
			}
		});

		$('#check_summary').val(arr.join(','));
	});

	$('#check_all').click(function(){
		if ($(this).is(':checked'))
		{
			var arr = new Array();
			$('.check').prop('checked', true).each(function(){
				arr.push($(this).attr('data-id'));
			});
			$('#check_summary').val(arr.join(','));
		}
		else
		{
			$('.check').prop('checked', false);
			$('#check_summary').val('');
		}
	});

	tinymce.init({
		selector: "textarea.mce",
		theme: "modern",
		plugins: [
			"advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
			"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
			"save table contextmenu directionality emoticons template paste textcolor responsivefilemanager"
		],
		toolbar1: "insertfile undo redo | styleselect | fontselect fontsizeselect | forecolor backcolor | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | emoticons",
		toolbar2: "| responsivefilemanager | link unlink anchor | image media | print preview code ",
		content_css: "/assets/plugins/ckeditor/contents.css",
		external_filemanager_path: "/fmgr/",
		filemanager_title: "Responsive Filemanager" ,
		external_plugins: { "filemanager" : "/fmgr/plugin.min.js"},
		font_formats: "Andale Mono=andale mono,times;"+
		"Arial=arial,helvetica,sans-serif;"+
		"Arial Black=arial black,avant garde;"+
		"Book Antiqua=book antiqua,palatino;"+
		"Comic Sans MS=comic sans ms,sans-serif;"+
		"Courier New=courier new,courier;"+
		"Georgia=georgia,palatino;"+
		"Helvetica=helvetica;"+
		"Impact=impact,chicago;"+
		"Symbol=symbol;"+
		"Tahoma=tahoma,arial,helvetica,sans-serif;"+
		"Terminal=terminal,monaco;"+
		"Times New Roman=times new roman,times;"+
		"Trebuchet MS=trebuchet ms,geneva;"+
		"Verdana=verdana,geneva;"+
		"Webdings=webdings;"+
		"Wingdings=wingdings,zapf dingbats",
		fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
		language: "ru",
		relative_urls: false
	});

})();